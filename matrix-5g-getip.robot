*** Settings ***
Documentation     Run tests on the matrix-5g-test experiment
Library           OperatingSystem
Library		  support.py
Resource	  matrix-5g.resource
Suite Teardown	  Close All Connections

*** Test Cases ***
Main
    SetGatewayIP
    IF	${DEBUG} == False
        # For Stackstorm, no newline. 
    	Log to Console	${GATEWAY}	no_newline=True
    END
