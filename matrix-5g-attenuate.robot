*** Settings ***
Documentation     Run tests on the matrix-5g-test experiment
Library           OperatingSystem
Library		  support.py
Resource	  matrix-5g.resource
Suite Teardown	  Close All Connections

*** Variables ***
# Command to attenuate.
${ATTENUATE}	/local/repository/bin/update-attens

# This will come in on the command line, otherwise back to zero.
${LEVEL}	0
# Also on the command line, might be more then one.
${AP}		ap1

*** Keywords ***
Attenuate
    [Arguments]			${ap}	${level}
    ${client}=		Run Remote Command
    ...				hostname=${CLIENT}
    ...				keyfile=${KEYFILE}
    ...				username=${USERNAME}
    ...				command=${ATTENUATE} ${ap} ${level}
    ${results}=		Get Remote Results	${client}
    Logit		${level}

*** Test Cases ***
Main
    Attenuate	${AP}	${LEVEL}
