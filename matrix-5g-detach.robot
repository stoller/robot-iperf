*** Settings ***
Documentation     Run tests on the matrix-5g-test experiment
Library           OperatingSystem
Library		  support.py
Resource	  matrix-5g.resource
Suite Teardown	  Close All Connections

*** Test Cases ***
Detach
    ${client}=		Run Remote Command
    ...				hostname=${CLIENT}
    ...				keyfile=${KEYFILE}
    ...				username=${USERNAME}
    ...				command=${DETACHCMD}
    ...				timeout=${TIMEOUT}
    ${results}=		Get Remote Results	${CLIENT}
    Logit		${results}
