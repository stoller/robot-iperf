*** Settings ***
Documentation     Run iperf between two nodes.
Library           OperatingSystem
Library		  support.py
Suite Teardown	  Close All Connections

*** Variables ***
${DEBUG}	True

# These will come in on the command line.
${CLIENT}	pc202
${SERVER}	pc203
${KEYFILE}      /users/stoller/.ssh/sut_rsa
${USERNAME}	stoller
${OUTPUT}	results

# Need to define the actual set of tests.
${SERVERCMD}	iperf3 -s -B server -p 45367
${CLIENTCMD}	iperf3 -c server -p 45367 -t 5 
# Two different iperf runs
${UPLOAD}	${CLIENTCMD}
${DOWNLOAD}	${CLIENTCMD} -R

*** Keywords ***
Start iPerf Server
    Run Remote Command		hostname=${SERVER}
    ...				keyfile=${KEYFILE}
    ...				username=${USERNAME}
    ...				command=${SERVERCMD}

Run Iperf Test
    [Arguments]			${command}	${output}
    ${client}=		Run Remote Command
    ...			hostname=${CLIENT}
    ...			keyfile=${KEYFILE}
    ...			username=${USERNAME}
    ...			command=${command}
    ${results}=		Get Remote Results	${client}
    IF	${DEBUG} == False
	Remove File	${output}
	Create File	${output}	${results}
    ELSE
        Logit	${results}
    END

Run Upload Test
    Run Iperf Test	${UPLOAD}	${OUTPUT}.upload

Run Download Test
    Run Iperf Test	${DOWNLOAD}	${OUTPUT}.download

Logit
    [Arguments]		${msg}
    # Newline
    Log to Console	\n	no_newline=True
    Log to Console	${msg}

# Abnormal exit make sure the ssh connections closed.
Close All Connections
    Terminate All Commands

*** Test Cases ***
Start Server
    Start iPerf Server

Upload Test
    Run Upload Test
    Sleep	2

Download Test
    Run Download Test
    Sleep	2




