#!/usr/bin/perl -w
#
# Copyright (c) 2000-2022 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

sub usage()
{
    print "Usage: dwell [-Qpi] [-f outputfile]\n";
    exit(1);
}
my $optlist   = "Qf:pi";
my $stdout    = 0;
my $doq       = 0;
my $doping    = 0;
my $doiperf   = 0;
my $SETATTEN  = "/local/repository/bin/update-attens";
my $QLOG      = "/local/QLog -f /local/defaultNR5G1216.cfg";
my $QLOGDIR   = "/var/tmp/qlog";
my $QLOGLOG   = "/var/tmp/qlog/qlog.log";
my $MINATTEN  = 0;
my $MAXATTEN  = 70;
my $ATTENSTEP = 1;
my $BASEATTEN = 56;
my $TTYDEV    = "/dev/ttyUSB2";
my $CHAT      = "chat -t 1 -sv '' AT OK";
my $CELLINFO  = 'AT+QENG=\"servingcell\"';
my $ATTACH    = 'AT+CFUN=1';
my $DETACH    = 'AT+CFUN=4';
my $CELLCMD   = "$CHAT '${CELLINFO}' OK < $TTYDEV > $TTYDEV";
my $ATTACHCMD = "$CHAT '${ATTACH}' OK < $TTYDEV > $TTYDEV";
my $DETACHCMD = "$CHAT '${DETACH}' OK < $TTYDEV > $TTYDEV";
my $PROXY     = "192.168.2.1";
my $PINGCMD   = "/bin/ping -i 0.5 -D -O $PROXY";
my $IPERFCMD  = "/bin/iperf3 -t 3600 -p 45367 -R -u -b24M -l 1300 -c $PROXY";
my $TRAFLOG   = "/var/tmp/traf.log";

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"Q"})) {
    $doq = 1;
}
if (defined($options{"i"})) {
    $doiperf = 1;
}
if (defined($options{"p"})) {
    $doping = 1;
}
if (defined($options{"f"})) {
    my $filename = $options{"f"};
    open(STDOUT, "> $filename") or die("opening $filename for STDOUT: $!");
    # No line buffering
    $| = 1; 
}

sub RunChat($)
{
    my ($chat)  = @_;
    my $command = "sudo /bin/sh -c \"$chat\"";
    my $output  = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	die("popen");
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
	if ($?) {
	    print STDERR $output;
	    return undef;
	}
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}

sub GetCellInfo()
{
    my $output = RunChat("$CELLCMD");
    if (!defined($output)) {
	return undef;
    }
    my @lines = split(/\n/, $output);
    while (@lines) {
	my $line = shift(@lines);
	if ($line =~ /NOCONN/) {
	    $line .= shift(@lines);
	    my @tokens = split(",", $line);
	    my $RSRP = $tokens[12];
	    my $RSRQ = $tokens[13];
	    my $SINR = $tokens[14];
	    return "RSRP:$RSRP, RSRQ:$RSRQ, SINR:$SINR";
	}
    }
    return undef;
}

RunChat("$ATTACHCMD");

#
# Fire off Qlog in a fork, kill when the test is over;
#
my $qpid;
if ($doq) {
    if (0) {
	if (-e $QLOGDIR) {
	    system("sudo /bin/rm -rf $QLOGDIR");
	}
    }
    system("/bin/mkdir $QLOGDIR") == 0 or die("mkdir $QLOGDIR")
	if (! -e $QLOGDIR);

    $qpid = fork();
    if (!$qpid) {
	open(STDERR, ">  $QLOGLOG") or die("opening $QLOGLOG for STDERR: $!");
	open(STDOUT, ">> $QLOGLOG") or die("opening $QLOGLOG for STDOUT: $!");
	# No line buffering
	$| = 1; 
	POSIX::setsid();
	exec("/bin/sudo $QLOG -s $QLOGDIR");
	exit();
    }
}

#
# Fire off a ping so we have the timestamps.
#
my $tpid;
if ($doping || $doiperf) {
    $tpid = fork();
    if (!$tpid) {
	open(STDERR, ">  $TRAFLOG") or die("opening $TRAFLOG for STDERR: $!");
	open(STDOUT, ">> $TRAFLOG") or die("opening $TRAFLOG for STDOUT: $!");
	# No line buffering
	select STDERR;
	$OUTPUT_AUTOFLUSH = 1;
	select STDOUT;
	$OUTPUT_AUTOFLUSH = 1;

	POSIX::setsid();
	if ($doping) {
	    exec("/bin/sudo $PINGCMD");
	}
	else {
	    exec("/bin/sudo $IPERFCMD --forceflush");
	}
	exit();
    }
}
my $start = time();

#
# Make sure will kill these things.
#
if ($doq || $doping) {
    my $killer = sub () {
	system("/bin/sudo /bin/kill -TERM $qpid")
	    if ($qpid);
	system("/bin/sudo /bin/kill -TERM $tpid")
	    if ($tpid);
	
	system("$SETATTEN 0");
	exit(1);
    };
    $SIG{INT}  = $killer;
    $SIG{TERM} = $killer;
    $SIG{QUIT} = $killer;
}

my $atten;

for ($atten = $MINATTEN; $atten <= $MAXATTEN; $atten += $ATTENSTEP) {
    my $actual = $BASEATTEN + $atten;
    my $now = time();
    my $sec = $now - $start;
    print "$now ($sec): Level $atten ($actual db)\n";

    system("$SETATTEN $atten");
    sleep(3);

    my $cellinfo = GetCellInfo();
    if (!defined($cellinfo)) {
	print "Could not get cell info, must be detached\n";
	last;
    }
    print "$cellinfo\n\n";
}

if (0) {
    RunChat("$DETACHCMD");
    sleep(2);
}
elsif ($atten > $MAXATTEN) {
    print "Never detached\n";
    if ($qpid) {
	system("/bin/sudo /bin/kill -TERM $qpid");
    }
    if ($tpid) {
	system("/bin/sudo /bin/kill -TERM $tpid");
    }
    system("$SETATTEN 0");
    exit(0);
}

print "\n";
print "Decreasing attenuation until attach succeeds.\n";
print "Sleeping for 5 seconds first.\n";
print "\n";
sleep(5);

while ($atten >= 0) {
    my $actual = $BASEATTEN + $atten;
    my $now = time();
    my $sec = $now - $start;
    print "$now ($sec): Level $atten ($actual db)\n";

    system("$SETATTEN $atten");
    sleep(2);
    RunChat("$ATTACHCMD");
    sleep(3);

    my $cellinfo = GetCellInfo();
    if (!defined($cellinfo)) {
	print "Could not cell info, must still be detached\n";
	$atten = $atten - 1;
	next;
    }
    print "Attach succeeded:\n";
    print "$cellinfo\n\n";
    print "Sleeping for 10 seconds.\n";
    print "\n";
    sleep(10);
    last;
}
system("$SETATTEN 0");
if ($qpid) {
    system("/bin/sudo /bin/kill -TERM $qpid");
}
if ($tpid) {
    system("/bin/sudo /bin/kill -TERM $tpid");
}
exit(0);
