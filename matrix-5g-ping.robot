*** Settings ***
Documentation     Run tests on the matrix-5g-test experiment
Library           OperatingSystem
Library		  support.py
Resource	  matrix-5g.resource
Suite Teardown	  Close All Connections

*** Variables ***
# Ping test. Gateway is known after attach.
${PINGCMD}      ping -c 10 -i 0.5

# Stash results in local file.
${PINGFILE}	${OUTPUT}.ping

*** Test Cases ***
Ping
    SetGatewayIP
    ${filename}=	Set Variable	${OUTPUT}.ping
    ${client}=		Run Remote Command
    ...				hostname=${CLIENT}
    ...				keyfile=${KEYFILE}
    ...				username=${USERNAME}
    ...				command=${PINGCMD} ${GATEWAY}
    ${results}=		Get Remote Results	${client}
    ${status}=		Get Remote Exit Status	${client}
    IF	${DEBUG} == False
	Remove File	${PINGFILE}
	Create File	${PINGFILE}	${results}
    ELSE
        Logit	${results}
    END
    IF	${status} != 0
        Fail
    END
