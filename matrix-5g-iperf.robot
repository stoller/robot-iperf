*** Settings ***
Documentation     Run tests on the matrix-5g-test experiment
Library           OperatingSystem
Library		  support.py
Resource	  matrix-5g.resource
Suite Teardown	  Close All Connections

*** Variables ***
${IPERF}	iperf3 -p 45367
${SERVERCMD}	${IPERF} -s
${CLIENTCMD}	${IPERF} -t 5 -u -b240M -l 1300

*** Keywords ***
Start iPerf Server
    Run Remote Command		hostname=${SERVER}
    ...				keyfile=${KEYFILE}
    ...				username=${USERNAME}
    ...				command=${SERVERCMD}

Run Iperf Test
    [Arguments]			${command}	${output}
    ${client}=		Run Remote Command
    ...			hostname=${CLIENT}
    ...			keyfile=${KEYFILE}
    ...			username=${USERNAME}
    ...			command=${command}
    ...			timeout=${TIMEOUT}
    ${results}=		Get Remote Results	${client}
    ${status}=		Get Remote Exit Status	${client}
    IF	${DEBUG} == False
	Create File	${output}	${results}
    ELSE
        Logit	${results}
    END
    IF	${status} != 0
        Fail
    END

Run Upload Test
    Run Iperf Test	${UPLOAD}	${OUTPUT}.upload

Run Download Test
    Run Iperf Test	${DOWNLOAD}	${OUTPUT}.download

*** Test Cases ***
Gateway IP
    SetGatewayIP

Set Test Variables
    Set Global Variable	\${SERVERCMD}	${SERVERCMD} -B ${GATEWAY}
    Set Global Variable	\${UPLOAD}	${CLIENTCMD} -c ${GATEWAY}
    Set Global Variable	\${DOWNLOAD}	${CLIENTCMD} -c ${GATEWAY} -R
    IF	${DEBUG} == True
        Log to Console	\n	no_newline=True
	Log to Console	${SERVERCMD}
	Log to Console	${UPLOAD}
	Log to Console	${DOWNLOAD}
    ELSE
        # So we have zero length files, not last run. But need a file.
	Remove File	${OUTPUT}.upload
	Create File	${OUTPUT}.upload
	Remove File	${OUTPUT}.download
	Create File	${OUTPUT}.download
    END

Start Server
    Start iPerf Server

Upload Test
    Run Upload Test
    Sleep	2

Download Test
    Run Download Test
    Sleep	2
